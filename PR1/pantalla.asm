; PRACTICA 1 - ARQUITECTURA Y ORGANIZACION DE COMPUTADORAS - G.Ing.Informatica
; SAMUEL ALFAGEME SAINZ - L1 - CURSO 2013/2014
segment .data

segment .bss
    printNum resb 11 ; Cadena que contendra el numero traducido a valores ASCII
    numInv resb 11 ; Cadena que representa el numero con las cifras invertidas
segment .text

global pantalla

pantalla:
    push rbp
    mov rbp,rsp
    mov rax, [rbp+16]
    xor ecx,ecx         ; ecx actua como contador de digitos
    cmp eax,0           ; Si el numero es negativo, tomaremos su valor absoluto
    jl setNeg
    jmp bucle
    setNeg:
        imul eax,-1     ; A = -A  ,si A < 0
        mov esi,1       ; El registro esi actua como flag para sumar el guion del numero
    bucle:
        xor edx,edx     ; Ponemos a 0 la parte superior del dividendo, eliminando el ultimo resto, cuando proceda
        cmp eax,10      ; Si A < 10, nos quedamos con el cocciente anterior, o el numero en si, almacenados en eax
        jl unidad
        mov ebx,10
        idiv ebx        ; El cocciente de la division edx:eax/10 sera el siguiente digito a guardar
        add dl,0x30     ; Calculo del valor ASCII correspondiente
        ; Guardado en la posicion i de la cadena invertida
        mov byte [numInv+ecx],dl    
        inc ecx         ; i++
        jmp bucle
    unidad:
        add al,0x30     ; Valor ASCII correspondiente al ultimo cocciente (digito de mas peso)
        ; Guardado en la ultima pos. de la cadena invertida:    
        mov byte [numInv+ecx],al    
        inc ecx         ; En ecx queda guardado el numero de digitos total del numero
        xor ebx,ebx
        cmp esi,1       ; En caso de que el numero sea negativo, incluimos los elementos necesarios
        je negativo
        jmp invertir
    negativo:
        mov byte [numInv+ecx],0x2D  ; En la ultima posicion del numero invertido guardamos el caracter guion
        inc ecx         ; Aumentamos el numero de caracteres del numero en 1 por esta razon
    invertir:
        ; Operacion de intercambio entre la cadena invertida y la que sera el string final
        mov al, byte[numInv+ecx]    
        ; Guardamos en la posicion j de printNum, el caracter de numInv[i], j:[0..len] i:[len..0]
        mov byte[printNum+ebx],al   
        inc ebx         ; j++
        dec ecx         ; i--
        cmp ecx,0       ; en caso de alcanzar el final de la cadena, pasamos a imprimirla
        jl imprimir
        jmp invertir    
    imprimir:           ; funcion que realiza la operacion de impresion del string
        mov eax, 4
        mov ebx, 1
        mov ecx, printNum ; La direccion de la cadena ya ordenada
        mov edx, 11     ; 11 bytes es la longitud max. del string convertido
        int 80h 
        mov ecx,0       ; ecx sera de nuevo un contador para limpiar el espacio de memoria,
    limpiar:            ; de manera que los resultados de una ejecucion no afecten a la siguiente.
        cmp ecx,11
        je salir
        ; Iniciamos byte a byte de los 11 de cada variable a 0
        mov byte [printNum+ecx],0 
        mov byte [numInv+ecx],0
        inc ecx
        jmp limpiar
    salir:
        xor esi,esi     ; Eliminamos negFlag, para las siguientes ejecuciones.
        mov rsp,rbp     
        pop rbp
        ret             ; Devolvemos el control al programa principal