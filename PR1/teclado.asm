; PRACTICA 1 - ARQUITECTURA Y ORGANIZACION DE COMPUTADORAS - G.Ing.Informatica
; SAMUEL ALFAGEME SAINZ - L1 - CURSO 2013/2014
segment .data

segment .bss
    numChar resb 11 ; esta variable almacenara la cadena ASCII correspondiente a cada numero 
    ; Al ser los numeros de tipo int, sus valores maximos son de 10 cifras, en el caso de ser negativo, 11
    ; al ocupar cada char, 1 byte, reservamos como maximo 11 bytes.
    acumulador resb 4 ; Al final de la funcion, la var. acumulador contendra el numero
    negFlag resb 4    ; Variable empleada como flag en caso de que el numero sea negativo (-1)
segment .text

global teclado
teclado:

    push rbp
    mov rbp, rsp
    ; Extraer la dir. de memoria de la variable int en la que se guardara la cadena tras convertirse
    ; dicha direccion se encontrara en el [SP+16]
    mov rdi, qword [rbp+16]
    ; Recogida de teclado:
    mov eax, 3               
    mov ebx, 0
    mov ecx, numChar
    mov edx, 11
    int 80h
    
    cmp byte [ecx], 0x2D     ; Comprobar si el numero es negativo (1er.car.Guion)
    je negativo
    mov eax, 1
    mov dword [negFlag], eax ; Si el numero es positivo, el negFlag sera 1
    jmp positivo             ; Transformar el numero.
    
    negativo:
        add ecx, 1           ;El guion no sera transformado en numero, lo saltamos.
        mov eax, -1
        mov dword [negFlag], eax ; En el caso negativo, negFlag sera -1

    positivo:
        ;Para conocer el orden de magnitud del numero, lo recorreremos para extraer su longitud:
        xor ebx, ebx     ; Variable de control(i), desde 0 hasta la lon. total del string
        xor eax, eax     ; Inicializar eax a 0, al sera donde se almacene cada caracter
        
        len: 
            mov al, [ecx+ebx] ; Al ser una cadena de char, cada elemento y el siguiente se diferencian en 1byte
            cmp eax,10       ; si el caracter fuese LF (salto de linea)
            je conversion    ; comienza a convertir los digitos con la long. guardada en ebx
            add ebx,1        ; si no es un salto de linea, se aumenta su longitud en 1
            jmp len

            ; En este punto, la direccion inicial de la cadena esta en el reg. ecx
            ; y la longitud de dicho string, guardada en ebx
        
        conversion:
            ; Inicializamos la variable a 0 para almacenar el resultado de la conversion:
            mov dword [acumulador], 0 
            xor edx,edx      ; int i = 0
            sub ebx,1        ; El indexado del array de caracteres se hace desde la posicion 0 hasta la n-1, resta para alcanzarla
            
            lectura:
                xor eax,eax       ; num = numChar[i]
                mov al,[ecx+edx]  ; cargar el byte en eax: numChar[i]
                sub al,0x30       ; varAux -= 48 (valor numerico correspondiente al caracter)

                xor esi,esi       ; int j = 0
                add esi,ebx       ; j = len.Cadena-1
                sub esi,edx       ; j = len.Cadena-1-i

                magnitud:         ; multiplica num por 10 hasta que concuerde con su orden de magnitud
                    cmp esi,0
                    je acumula    ; Salta si j = 0 (La magnitud de num es correcta)
                    imul eax,10   ; varAux = varAux * 10
                    dec esi       ; j--
                    jmp magnitud
                
                acumula:
                    add dword [acumulador],eax ; Se suma num al acumulador 
                    cmp edx,ebx        
                    je salida     ; i = len.Cadena-1 - en el momento que esto sea cierto, el numero se habra recorrido completamente
                    inc edx       ; i++
                    jmp lectura
        
salida:
    mov eax,[acumulador]
    imul eax, [negFlag]     ; si el numero es negativo: *(-1), en caso contrario *(1)
    mov dword [edi],eax     ; por ultimo, guardamos el numero en la direccion pasada como parametro
    mov rsp,rbp
    pop rbp                 ; restauramos el valor original del apuntador de pila
    ret                     ; y devolvemos el control al programa principal
