PR2 - ARQUITECTURA Y ORGANIZACIÓN DE COMPUTADORES
=================================================
Descripción:
------------
Escritura de una función en ensamblador para IA-32 que, a partir del valor de la variable real de punto flotante `x`, calcule el valor de `y` por la expresión:

![equation](http://latex.codecogs.com/gif.latex?y%20=%2010^{x}\cdot\frac{\sqrt{1+x^{2}}}{sin%28x%29})

La interacción con el usuario, se realiza insertando dicha función en un fragmento de código escrito en **C** y el paso/retorno de valores entre la función en ensamblador y el lenguaje de alto nivel se efectúa mediante variables globales.
### Técnicas empleadas:
* Manejo del conjunto de instrucciones del repertorio **x87** (FP)
* Inserción de una función escrita en ensamblador en código de alto nivel.

Requisitos:
-----------
* [NASM]: the Netwide Assembler.

Ejemplo de ejecución:
---------------------
### Compilación:
```
nasm -f elf64 calculo.asm
```
### Enlazado:
```
gcc -o PR2 PR2.c calculo.o 
```
### Ejecución:
```
./PR2
PR2 - AOC : Calculo de una funcion con variables reales
Introduzca el valor de x: 1.34
El valor calculado para y: 37.58
```
Licencia:
---------
**Samuel Alfageme Sainz**

*Práctica 2 - Arquitectura y Organización de Computadoras*

*Grado en Ingeniería Informática - UVa*

MIT.

[NASM]: http://www.nasm.us/