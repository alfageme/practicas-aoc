global calculo

extern x
extern y

segment .bss
	; Palabra de control usada para cambiar a truncamiento
	cword resw 1
	; Variable auxiliar que almacenara la primera parte del calculo
	; para que el codigo sea mas legible.
	op1 resd 1

segment .text
calculo:

	; st0 = x 
	fld dword[x]
	; st0 = x^2
	fmul st0, st0
	; st0 = (x^2 + 1)
	; la combinacion de instrucciones fld1/faddp permite realizar
	; el incremento manteniendo igual el contenido de la pila.
	fld1
	faddp
	; st0 = sqrt(x^2 + 1)
	fsqrt

	; st0 = x
	fld dword[x]
	; st0 = sin(x)
	fsin

	; st1 = sqrt(x^2 + 1)/sin(x) = M[op1]
	fdiv st1,st0
	fxch
	fst dword[op1]
	
	; Modifica los 2 bits de control de redondeo, para truncar:
	; Carga la palabra de control
	fstcw word[cword]
	; Pone los 2 bits de control de redondeo a 11 
	or word[cword], 0x0C00
	; Guarda la nueva palabra de control
	fldcw word[cword]

	; st0 = log2(10) 
	fldl2t
	; st0 = x * log2(10)
	fmul dword[x]
	; st0 = x * log2(10) , st1 = duplicado de st0 
	fld st0
	
	; E = parte entera del exponente.
	; D = parte decimal del exponente

	; Extraemos la parte entera del exponente del 10:
	; st0 = E
	frndint
	; st1 = exponente - E = D
	fsub st1, st0
	; st0 = D , st1 = E
	fxch
	; st0 = 2^(D) - 1
	f2xm1
	; st0 = 2^(D)
	fld1
	faddp
	; st0 = E , st1 = 2^(D)
	fxch
	; st0 = 1.0 , st1 = E , st2 = 2^(D)
	fld1
	; st0 = 1.0 * 2^(E)
	fscale
	; st0 = 2^(D) * 2^(E) = 2^(x*log2(10))
	fmul st0, st2
	; st0 = sqrt(x^2 + 1)/sin(x) * 2^(x*log2(10)) = 10^x * sqrt(x^2 + 1)/sin(x)
	fmul dword[op1]
	; y = st0
	fst dword[y]
	; retorno al programa principal:
	ret
