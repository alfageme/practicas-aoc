PR3 - ARQUITECTURA Y ORGANIZACIÓN DE COMPUTADORES
=================================================
Descripción:
------------
Serie de Maclaurin

![equation](http://latex.codecogs.com/gif.latex?cos%28x%29%20=%201%20-%20\frac{x^{2}}{2!}%20+%20\frac{x^{4}}{4!}%20-%20\frac{x^{6}}{6!}%20+%20...%20=%20\sum_{n=0}^{\infty}%28-1%29^{n}\frac{x^{2n}}{%282n%29!})

### Técnicas empleadas:


Requisitos:
-----------
* [NASM]: the Netwide Assembler.

Licencia:
---------
**Samuel Alfageme Sainz**

*Práctica 3 - Arquitectura y Organización de Computadoras*

*Grado en Ingeniería Informática - UVa*

MIT.

[NASM]: http://www.nasm.us/