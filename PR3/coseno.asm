segment .bss
    cosin       resd 1

segment .data
    precision   dd 0.000001
    x           dd 1.55

segment .text
    global _start:
_start:
    fld dword[precision]
    fstp st5                ; st5 = precision |?
    fld dword[x]            ; st6 = precision |?
    fmul st0,st0
    fstp st4                ; st3 = x^2 st5 = precision     
    ; Se guarda el primer termino de la serie: invariante.
    fld1                    ; st0 = 1 st4 = x^2 st6 = precision
    fst st2                 ; st2 = 1
    ;fst dword[cosin]
    fchs                    ; st0 = -1 st2 = 1 
    fstp st3                ; 
    fldz
    fst st1
    fld st2
nexterm:
    xor esi,esi
    fabs
    fcomi st6
    jb guardado
    fmul st0,st5
    ;fst st3            ; st0 = x^n * x^2 = x^(n+2)
factorial:
    fld1
    faddp st3,st0
    inc esi
    fdiv st0,st2 
    cmp esi,2
    jl factorial
    ; st0 = termino x^n/n!
    fmul st0,st4    ; DARLE EL SIGNO CORRESPONDIENTE AL FACTOR 
    fld1            ; | 
    fchs            ; | CAMBIAR EL SIGNO PARA EL SIGUIENTE FACTOR
    fmulp st5,st0   ; |
    fadd st3,st0; ANADIR EL FACTOR AL RESULTADO CALCULADO HASTA AHORA
     ; ESTO FUNCIONA SI LAS EFLAGS ESTAN BIEN
    jmp nexterm     ; calcular siguiente termino
guardado:
    fld st3
    fst dword[cosin]
salida:
    mov eax, 1
    mov ebx, 0
    int 80h