//#include <stdio.h>
//#include <stdlib.h>

int main(){
	float x = 1.55;
	float precision = 1E-6;
	float coseno,lastTerm;
	//printf("Introduzca el valor positivo cuyo coseno quiere calcular: ");
	//scanf("%f",&x);
	//printf("Introduzca la precision con la que desea calcular la serie: ");
	//scanf("%f",&precision);
	// Comprobar que los valores introducidos son correctos
	/*
	if(x<0)
		exit(1);
	if((precision<1e-8)||(precision>1))
		exit(2);
	*/
	float x2 = x*x;
	int num = 0;
	int sign = -1;
	lastTerm = 1;
	coseno = lastTerm;

	while(lastTerm>precision){
		lastTerm = lastTerm * x2;
		int i;
		for(i=0;i<2;i++){
			num++;
			lastTerm=lastTerm/num;
		}
		coseno = coseno + (lastTerm*sign);
		sign = sign * -1;
	}
	//printf("Valor calculado: cos(%f)=%.8f\n",x,coseno);
	return 0;
}